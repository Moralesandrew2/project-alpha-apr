from django.shortcuts import render
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("show_project", id=task.project.id)
    else:
        form = TaskForm()
        context = {
            "form": form,
        }
    return render(request, "tasks/create_task.html", context)


@login_required
def task_list(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": task_list,
    }
    return render(request, "tasks/list.html", context)
